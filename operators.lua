local Operators = {
	["put"] = function(catlang, line)
		for i = 1, line.arguments_count do
			table.insert(catlang.stack, line.arguments[i])
		end
	end,
	["sum"] = function(catlang, line)
		local sum = 0
		if line.arguments_count == 1 then
			for i = 1, line.arguments[1] do
				sum = sum + (table.remove(catlang.stack) or 0)
			end
		elseif line.arguments_count > 1 then
			for i = 1, line.arguments_count do
				sum = sum + line.arguments[i]
			end
		end
		table.insert(catlang.stack, sum)
	end,
	["print"] = function(catlang, line)
		print("> "..(table.remove(catlang.stack) or "no value in stack!"))
	end,
	["stop"] = function(catlang)
		catlang.run = false
	end,
	["wait"] = function(catlang)
		io.write("Press 'Enter'") io.read() io.write("\n")
	end
}

return Operators
