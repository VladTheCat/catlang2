local Operators = require "operators"

local Catlang = {
	operators = Operators,
	code_file = nil,
	code_lines = {},
	current_line = 1,
	stack = {},
	run = false
}

function Catlang.load_file(catlang, file)
	local fl = io.open(file, "r")
	local lines = {}
	local cur_line = 1
	if fl then
		for line in fl:lines() do
			local char_n = 1
			local char = string.sub(line, 1, 1)
			local temp_word = ""
			local op = nil
			local args = {}
			local args_count = 0
			local stage = 0
			local str = false
			local str_char = ""
			if char ~= "!" then
				for j = 1, #line do
					char = string.sub(line, j, j)
					if stage == 0 then
						if char ~= " " then
							if char ~= ":" and char ~= ";" then
								temp_word = temp_word .. char
							elseif char == ":" then
								op = temp_word
								temp_word = ""
								stage = 1
							elseif char == ";" then
								op = temp_word
								temp_word = ""
								break
							end
						end
					else
						if not str then
							if char ~= " " then
								if char ~= "," and char ~= ";" then
									if char == "'" or char == '"' then
										str = true
										str_char = char
									else
										temp_word = temp_word .. char
									end
								else
									args[stage] = tonumber(temp_word)
									temp_word = ""
									stage = stage + 1
									args_count = args_count + 1
									if char == ";" then break end
								end
							end
						else
							if char ~= str_char then
								temp_word = temp_word .. char
							else
								args[stage] = temp_word
								temp_word = ""
								stage = stage + 1
								str = false
								args_count = args_count + 1
							end
						end
					end
				end
				catlang.code_lines[cur_line] = { operator = op, arguments = args, arguments_count = args_count}
				cur_line = cur_line + 1
			end
		end
	end

	for i = 1, #catlang.code_lines do
		io.write(string.format("Operator: %s; Arguments:", catlang.code_lines[i].operator))
		for j = 1, #catlang.code_lines[i].arguments do
			io.write(string.format(" %s", catlang.code_lines[i].arguments[j]))
		end
		io.write(";\n")
	end
	fl:close()
end

function Catlang.execute(catlang)
	catlang.run = true
	catlang.current_line = 1
	while catlang.run do
		local line = catlang.code_lines[catlang.current_line]
		local op = line.operator
		local args = line.arguments
		if catlang.operators[op] ~= nil then
			catlang.operators[op](catlang, line)
		else
			print("Error: unexpected operator")
		end
		catlang.current_line = catlang.current_line + 1
	end
end


return Catlang